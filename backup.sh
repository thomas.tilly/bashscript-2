#!/bin/bash

SRC_DIRECTORY="/Users/$USER"
DEST_DIRECTORY=/var/backup/
USER=$(whoami)
DATE=$(date +%d%m%Y)
HEURE=$(date +"%H:%M")

BACKUP_FILE=$USER.$DATE.$HOUR.backup.gz