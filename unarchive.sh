#!/bin/bash

files=$*

if [ "$#" -lt 1 ]; then
    echo "Il faut rentrer au moins un argument"
    echo "Pour vous aider voici une commande : ./unarchive.sh [--help]"
elif [ "$1" == "--help" ]; then
    echo "Pour utiliser la commande il suffit de faire simplement: ./unarchive.sh [fichier1] [fichier2] [fichier...]"
else
    for files in $@; do
        if [[ $files != *.gz ]]; then
            echo "$i : Pas un fichier .gz à décompresser"
        else
            for i in *.gz; do
                gunzip $i; done
        fi
        if [[ $files != *.zip ]]; then
            echo "$i : Pas un fichier .zip à décompresser"
        else
            for i in *.zip; do
                unzip $i; done
        fi
        if [[ $files != *.bz2 ]]; then
            echo "$i : Pas un fichier .bz2 à décompresser"
        else
            for i in *.bz2; do
                bunzip2 $i; done
        fi
        if [[ $files != *.rar ]]; then
            echo "$i : Pas un fichier .rar à décompresser"
        else
            for i in *.rar; do
                unrar x $i; done
        fi
        if [[ $files != *.xz ]]; then
            echo "$i : Pas un fichier .xz à décompresser"
        else
            for i in *.xz; do
                tar zxvf $i; done
        fi
    done
    echo ""
    echo ""
    echo ""
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "Tous vos fichiers ont été décompressés avec succès"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
fi