#!/bin/bash

rep=$1

if [ "$#" -gt 1 ]; then
        echo "Veuillez ne rentrer qu'un argument ou aucun."
elif [ "$1" == "--help" ]; then
        echo "Pour utiliser la commande il suffit de faire simplement: ./high5.sh [nom_du_dossier]"
elif [[ ! -d $rep ]]; then
    echo "Il faut rentrer un répertoire valide..."
    echo "Pour vous aider voici une commande : ./high5.sh [--help]"
elif [[ ! -z $rep ]] && [[ -z "$2" ]]; then
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "Voici les 5 fichiers les plus volumineux: "
    ls -lS $rep | head -6
    echo " "
    echo "Voici les 5 fichiers les plus récents: "
    ls -lt $rep | head -6
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
fi